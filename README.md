#Пробный бот для Telegram

    git clone git@bitbucket.org:dimsharav/telegrambot.git
    virtualenv -p python3 virtualenv
    source virtualenv/bin/activate.sh
    pip install -r requirements.txt

    # Autostart
    cp telegrambot.service.example telegrambot.service
    vim telegrambot.service     # and modify path to virtualenv
    ln -s `pwd`telegrambot.service /etc/systemd/system/telegrambot.service
    systemctl start telegrambot
    systemctl enable telegrambot
  
Источники:

- Первоначально (устар.) http://groosh-code.tumblr.com/post/127734971438/пишем-бота-для-telegram-урок-1-введение-простой
- https://www.gitbook.com/read/book/kondra007/telegram-bot-lessons
