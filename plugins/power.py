#!/usr/bin/python3

from wakeonlan import send_magic_packet


def power_on():
    send_magic_packet('00:50:22:8b:3b:81')


if __name__ == "__main__":
    power_on()
