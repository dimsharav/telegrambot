#!/usr/bin/python3

import subprocess

ansible_workdir = '/home/ansible/ansible/'
ansible_bin = ansible_workdir + 'env/bin/ansible-playbook'


def ansible_playbook(playbook):
    playbook = ansible_workdir + 'playbooks/' + playbook
    command = ansible_bin + ' ' + playbook
    p = subprocess.Popen(command, cwd=ansible_workdir, shell=True,
                         stdout=subprocess.PIPE)
    out = p.stdout.read()
    return out.decode('utf-8')
