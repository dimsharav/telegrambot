#!/usr/bin/python3
""" Bot for home """

import config
import telebot
from telebot import types
from telebot import apihelper
import plugins.power
import plugins.ansible

if config.proxy != {}:
    apihelper.proxy = config.proxy
bot = telebot.TeleBot(config.token)
logger = telebot.logger
telebot.logger.setLevel(config.log_level)


def auth(id):
    return id in config.allowed_id


def work_markup():
    markup = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    btn_work_on = types.KeyboardButton('/work_on')
    btn_work_off = types.KeyboardButton('/work_off')
    markup.add(btn_work_on, btn_work_off)
    return markup


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    logger.info(message)
    bot.send_message(message.chat.id, 'Тестовый бот')


@bot.message_handler(commands=['work'])
def work(message):
    logger.info(message)
    if auth(message.chat.id):
        bot.send_message(message.chat.id, 'Что сделать?', reply_markup=work_markup())


@bot.message_handler(commands=['work_on'])
def work_on(message):
    logger.info(message)
    if auth(message.chat.id):
        bot.reply_to(message, 'Сейчас включу')
        plugins.power.power_on()
        bot.reply_to(message, 'Сделано')
    else:
        logger.warn('Не авторизован')


@bot.message_handler(commands=['work_off'])
def work_off(message):
    logger.info(message)
    if auth(message.chat.id):
        bot.reply_to(message, 'Сейчас выключу')
        bot.reply_to(message, plugins.ansible.ansible_playbook('shutdown.yml'))
        bot.reply_to(message, 'Сделано')
    else:
        logger.warn('Не авторизован')


@bot.message_handler(func=lambda message: True)
def listener(message):
    logger.info(message)
    bot.reply_to(message, 'Не понимаю')


if __name__ == '__main__':
    bot.polling(none_stop=True)
